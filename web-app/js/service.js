let service = {

    url: 'http://localhost:8080/todos',

    getTodos(page) {
        return $.getJSON(this.url + '/' + page);
    },

    getTodo(id) {
        let params = {id: id};
        return $.getJSON(this.url, params);
    },

    addTodo(todo) {
        return $.ajax({
            type: "POST",
            data: JSON.stringify(todo),
            url: this.url,
            contentType: "application/json; charset=utf-8",
            success: () => toastr["success"]("Todo added"),
            statusCode: {
                400: data => toastr["info"](data.responseJSON.errors[0].defaultMessage),
                404: () => toastr["info"]('not found'),
                500: () => toastr["info"]('internal server error'),
                503: () => toastr["info"]('service unavailable'),
            }
        });
    },

    deleteTodo(id) {
        return $.ajax({
            type: "DELETE",
            url: this.url + '/' + id,
            contentType: "application/json; charset=utf-8",
            success: () => toastr["success"]("Todo deleted"),
        });
    },

    updateTodo(todo) {
        return $.ajax({
            type: "PUT",
            data: JSON.stringify(todo),
            url: this.url,
            contentType: "application/json; charset=utf-8",
            success: () => toastr["success"]("Todo updated"),
            statusCode: {
                400: data => toastr["info"](data.responseJSON.errors[0].defaultMessage),
                404: () => toastr["info"]('not found'),
                500: () => toastr["info"]('internal server error'),
                503: () => toastr["info"]('service unavailable'),
            }
        });
    }

};
