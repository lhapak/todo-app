let paginationController = {

    currentPage: 1,
    todosCount: 0,
    pageSize: 7,
    nextPageIcon: $("#next-page-icon"),
    previousPageIcon: $("#previous-page-icon"),

    previousPage() {
        if (this.currentPage === 2) {
            this.disablePreviousPageIcon();
        }
        if (this.currentPage > 1) {
            this.currentPage--;
            $("#container").addClass("swipe-left");
            setTimeout(function () {
                $("#container").removeClass("swipe-left");
            }, 500);
            listController.refresh();
        }
    },

    nextPage() {
        if (this.currentPage * this.pageSize < this.todosCount) {
            if (this.currentPage === 1) {
                this.enablePreviousPageIcon();
            }
            this.currentPage++;
            $("#container").addClass("swipe-right");
            setTimeout(function () {
                $("#container").removeClass("swipe-right");
            }, 500);
            listController.refresh();
        }
    },

    disableNextPageIcon() {
        this.nextPageIcon.addClass("disabled");
        this.nextPageIcon.removeClass("pagination-icon");
    },

    enableNextPageIcon() {
        this.nextPageIcon.removeClass("disabled");
        this.nextPageIcon.addClass("pagination-icon");
    },

    disablePreviousPageIcon() {
        this.previousPageIcon.addClass("disabled");
        this.previousPageIcon.removeClass("pagination-icon");
    },

    enablePreviousPageIcon() {
        this.previousPageIcon.removeClass("disabled");
        this.previousPageIcon.addClass("pagination-icon");
    },
};