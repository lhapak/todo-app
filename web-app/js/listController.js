let listController = {

    deleteTodo(id, htmlElement) {
        $.when(service
            .deleteTodo(id))
            .done(() => {
                htmlElement.parentElement.classList.add("flip-out-x");
                setTimeout(function () {
                    listController.refresh();
                }, 600);
            });
    },

    renderTodos(todos) {
        $("#todo-list")
            .html(todos
                .map(x => this.generateTodoComponent(x))
                .join(''));
    },

    generateTodoComponent(todo) {
        return `<li>
        <i onclick="modalController.editTodo(${todo.id})" class="edit-icon fas fa-edit"></i>
            ${todo.title}
        <i onclick="listController.deleteTodo(${todo.id}, this)" class='delete-icon fas fa-trash'></i>
     </li>`
    },

    refresh() {
        $.when(service
            .getTodos(paginationController.currentPage))
            .done(data => {
                if (data.numberOfElements === 0) {
                    paginationController.previousPage();
                }
                this.renderTodos(data.content);
                paginationController.todosCount = data.totalElements;
                paginationController.pageSize = data.size;
                if (data.last) {
                    paginationController.disableNextPageIcon();
                } else {
                    paginationController.enableNextPageIcon();
                }
            });
        $("#todo-title").val("");
    }
};

listController.refresh();