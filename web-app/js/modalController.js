let modalController = {

    modalBg: $("#edit-modal-bg"),
    modal: $("#edit-modal"),

    openEditModal(todo) {
        $("#edit-modal-bg").show();
        $("#edit-modal-content").html(
            `<form action="javascript:modalController.updateTodo(${todo.id})" id="edit-todo">
                <input value="${todo.title}" type="text" id="edited-todo-title" placeholder="Title..." required minlength="3" maxlength="20">
                <button>Confirm Edit</button>
            </form>`);
    },

    closeEditModal() {
        this.modal.addClass("fade-out-and-scale");
        this.modalBg.addClass("fade-out");
        setTimeout(function () {
            modalController.modal.removeClass("fade-out-and-scale");
            modalController.modalBg.removeClass("fade-out");
            modalController.modalBg.hide();
        }, 600);
    },

    editTodo(id) {
        $.when(service
            .getTodo(id))
            .done(data => this.openEditModal(data));
    },

    updateTodo(id) {
        let todo = {
            id: id,
            title: $("#edited-todo-title").val()
        };
        $.when(service
            .updateTodo(todo))
            .done(() => {
                this.closeEditModal();
                listController.refresh();
            })
    },

    addEventListener() {
        window.onclick = (event) => {
            if (event.target === document.querySelector('#edit-modal-bg')) {
                modalController.closeEditModal();
            }
        };
    }
};

modalController.addEventListener();