let formController = {

    addTodo() {
        let todo = {title: $("#todo-title").val()};
        $.when(service
            .addTodo(todo))
            .done(() => listController.refresh());
    }

};