# Todo App

Basic todo application: Rest Api + web client

### Backend :
```
Java, Spring Boot, Jpa, PostgreSQL
```
### Frontend:
```
HTML, CSS, Javascript, JQuery
```