package com.example.todo;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name = "Todo")
class Todo {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Title cannot be null")
    @Size(min = 3, max = 20, message = "Title length must be between 3 and 20 characters")
    @Column(name = "TITLE")
    private String title;

}
