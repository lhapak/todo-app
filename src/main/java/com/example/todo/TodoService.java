package com.example.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TodoService {

    private TodoRepository repository;

    @Autowired
    public TodoService(TodoRepository repository) {
        this.repository = repository;
    }

    Page<Todo> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    Optional<Todo> getById(int id) {
        return repository.findById(id);
    }

    void addTodo(Todo todo) {
        repository.save(todo);
    }

    void updateTodo(Todo todo) {
        repository.save(todo);
    }

    void deleteTodo(int id) {
        repository.deleteById(id);
    }
}
