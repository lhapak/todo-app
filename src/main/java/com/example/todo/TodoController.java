package com.example.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private TodoService service;

    @Autowired
    public TodoController(TodoService service) {
        this.service = service;
    }

    @GetMapping("/{page}")
    @CrossOrigin
    public Page<Todo> getAll(@PathVariable int page) {
        Pageable pageable = PageRequest.of(page - 1, 7, Sort.Direction.ASC, "id");
        return service.getAll(pageable);
    }

    @GetMapping
    @CrossOrigin
    public Optional<Todo> getById(@RequestParam int id) {
        return service.getById(id);
    }

    @PostMapping
    @CrossOrigin
    public void addTodo(@Valid @RequestBody Todo todo) {
        service.addTodo(todo);
    }

    @PutMapping
    @CrossOrigin
    public void updateTodo(@Valid @RequestBody Todo todo) {
        service.updateTodo(todo);
    }

    @DeleteMapping("/{id}")
    @CrossOrigin
    public void deleteTodo(@PathVariable int id) {
        service.deleteTodo(id);
    }
}
